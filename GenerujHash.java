/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 *
 * @author student
 */
@WebServlet(name = "GenerujHash", urlPatterns = {"/GenerujHash"})
public class GenerujHash extends HttpServlet {

    public GenerujHash(){
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        doPost(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String url = "/Wyswietl.jsp";
        
        String toHash = request.getParameter("inputString");
        String computedHash = Classes.HashCom.SHA384(toHash);
        request.setAttribute("computedHash", computedHash);
        
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }
}